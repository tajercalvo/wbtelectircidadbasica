$(document).ready(function () {
    $("#footer").load("src/footerSetion.html", function () {
        InvocaNombreUsr();
    });
   
});
    var Secciones = ["section0.html","pregunta1.html"];
    var Avance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var CantModules = Avance.length;
    var CantidadPreguntas = 10;

function Ir(Modulo) {
    if (Modulo == 0) {
        $('.content').css('background-color', '#2b2e36');
        $('section footer').css('background-color', '#1e2126');
        $('.collapse').css('visibility', 'visible')


    } else {
        $('.content').css('background-color', 'white');
        $('section footer').css('background-color', 'rgb(43 46 53)');
        $('.collapse').css('visibility', 'hidden')
    }

    $('#contenido').removeClass('d-flex');
    $('#contenido').removeClass('col-12');
    $('#contenido').addClass('col-11');
    $('.navSection').css('display', 'block');
    $('.content').css('height', 'auto');
    $('.bordesolido').removeClass('btnSalir');
    $('.bordesolido').addClass('text_dorado')
    $('.tituloCabezera').css('visibility', 'visible')
    if (Modulo > 0) {
            Avance[0] = 1;
            $(".conModulo").load(Secciones[Modulo]);
      }else {
        $(".conModulo").load(Secciones[Modulo]);
      }
    CalcPorcentaje();
}

function CalcPorcentaje() {
    var vistos = 0;
    for (var i = 0; i < Avance.length; i++) {
        if (Avance[i] == 1) {
            vistos++;
        }
    }
    porcentaje = Math.round((vistos / CantModules) * 100);
    $('.progress-bar').text(porcentaje + ' %');
    $('.progress-bar').css('width', porcentaje + '%');
    if (porcentaje == 100) {
        $('#finalizado').val("SI"); //Esto es para arreglarlo.
    }
}

var CantCorrectas = 0;
var preguntasVistas = 0;

function IrPre(pre, op) {
     CalcPorcentaje();
    if (pre == 1) {
        $("#contenido").load('pregunta2.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 4) {
            CantCorrectas++;
        }
    } else if (pre == 2) {
        $("#contenido").load('pregunta3.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 1) {
            CantCorrectas++;
        }
    } else if (pre == 3) {
        $("#contenido").load('pregunta4.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 2) {
            CantCorrectas++;
        }
    }else if (pre == 4) {
        $("#contenido").load('pregunta5.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 2) {
            CantCorrectas++;
        }
    }else if (pre == 5) {
        $("#contenido").load('pregunta6.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 2) {
            CantCorrectas++;
        }
    }else if (pre == 6) {
        $("#contenido").load('pregunta7.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 4) {
            CantCorrectas++;
        }
    }else if (pre == 7) {
        $("#contenido").load('pregunta8.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 1) {
            CantCorrectas++;
        }
    }else if (pre == 8) {
        $("#contenido").load('pregunta9.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 2) {
            CantCorrectas++;
        }
    }else if (pre == 9) {
        $("#contenido").load('pregunta10.html');
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 1) {
            CantCorrectas++;
        }
    }else if (pre == 10) {
        preguntasVistas++;
        Avance[pre] = 1;
        if (op == 2) {
            CantCorrectas++;
        }

        

        if (preguntasVistas >=10) {

            var ResTot = 50 + Math.round((CantCorrectas / CantidadPreguntas) * 50);
            $("#ResEvaluacion").val(ResTot);
            if (ResTot > 75) {
                $("#contenido").load('resultado.html');
                $('.content').css('background-color', '#2b2e36');
                $('section footer').css('background-color', '#1e2126');
                $('.collapse').css('visibility', 'visible')

                $("#tituloAlerta").html("¡Resultado!");
                $("#TextoAlerta").html("Felicitaciones " + CantCorrectas + " respuestas correctas de " + CantidadPreguntas + ". Su puntaje para este curso es de: " + ResTot + " Puntos.<br>Precione el Boton Fin Para Terminar el Curso");
                $("#basic-modal-content").modal();
                $('.tres').hide();
                $('#btnFin').show();
                //se hace visible el boton de fin(cuando precione el boton fin ejecutA LA FUNCION Irpre('fin')) y el texto de felicitaciones
            }else{
                //no paso
                $("#contenido").load('incorrecto.html');
                $('.tres').text("SALIR");
                $('#btnFin').hide()
                $('#contenidoSection').hide()
                $("#tituloAlerta").html("¡Resultado!");
                $("#TextoAlerta").html("Lo sentimos Su puntaje para este curso es de: " + ResTot + " Puntos. <br>Respuestas correctas "+ CantCorrectas + " de " + CantidadPreguntas + "<br>No olvide dar clic en SALIR en la parte superior derecha para registrar el intento en la plataforma");
                $("#basic-modal-content").modal();
                $('#rph5').prop("disabled", true); 

                
            }
         }

    }else if(pre == "fin"){
        $('.wrapper').load('fin.html')
        $('.footer').css('display', 'none')
        $('.wrapper').css("background-color", "#2b2e36");
        $('.tres').show();
        $('.tres').text("SALIR");
        $("#tituloAlerta").html("¡Importante!");
        $("#TextoAlerta").html("No olvide dar clic en SALIR en la parte superior derecha para registrar el intento en la plataforma");
        $("#basic-modal-content").modal();

    }

}
$('#SalirBtn').click(function (e) {
    $('#precarga').show();
    //completaCourse(1);
});