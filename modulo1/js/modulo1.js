// Copyright 1999-2021. Plesk International GmbH. All rights reserved.


$(document).ready(function() {
    $('#precarga').hide();
    //jQuery(window).load(function () {
    // $('#precarga').hide();
    // });
    //// fin ----ingresar en lo js
});

var sect = ['section0.html', 'section1.html'],
    avance = [0, 0, ],
    cantSec = avance.length;

$('body').on('click', '.continuar', function(e) {
    e.preventDefault();
    $('.btnIni').css('display', 'none');
    $('.btnSalirModulo').css('display', 'block');
    $('.conModulo').load('iniciar.html', function() {
        InvocaNombreUsr();
    });
    $('.navSection').load('src/menu-contenido1.html');
    $('#precarga').show();

});

function Ir(Modulo) {
    $('.collapse').css('visibility', 'visible')
    $('#contenido').removeClass('d-flex');
    $('#contenido').removeClass('col-12');
    $('#contenido').addClass('col-11');
    $('.navSection').css('display', 'block');
    $('.content').css('height', 'auto');
    $('.bordesolido').removeClass('btnSalir');
    $('.bordesolido').addClass('text_dorado')
    $('.tituloCabezera').css('visibility', 'visible')
    $('.content').css('background-color', 'white');
    $('section footer').css('background-color', 'rgb(43 46 53)');

    if ($("li").hasClass("MAnimacion")) {
        $('#tituloAlerta').html('¡Alerta!');
        $('#TextoAlerta').html('Para ver la siguiente sección debe completar esta sección');
        $('#basic-modal-content').modal();
    } else {
        if (Modulo > 0) {

            if (avance[Modulo - 1] == 1) {
                avance[Modulo] = 1;
                //$('#Container').html('<div class="loading"><img src="images/loading.gif" width="110px"></div>');
                $('.conModulo').load(sect[Modulo]);
                $('.lista i.menuActivo').removeClass('menuActivo');
                $('#VinI' + Modulo + ' i').addClass('menuActivo');
            } else {
                $('#tituloAlerta').html('¡Alerta!');
                $('#TextoAlerta').html('Para ver esta sección debe completar la sección anterior, la navegación debe ir en forma secuencial.');
                $('#basic-modal-content').modal();
            }
        } else {
            avance[Modulo] = 1;
            //$('#Container').html('<div class="loading"><img src="images/loading.gif" width="110px"></div>');
            $('.conModulo').load(sect[Modulo]);
            $('.lista i.menuActivo').removeClass('menuActivo');
            $('#VinI' + Modulo + ' i').addClass('menuActivo');
        }
    }
    CalcPorcentaje();
}

// var CantCorrectas = 0;
// var valida1 = 0;
// var valida2 = 0;

// function pre(pre, op) {
//     if (pre == 1) {
//         valida1 = 1;
//         $('.conPre').load('preguntas2.html');
//         if (op == 3) {
//             CantCorrectas++;
//         }
//     } else if (pre == 2) {
//         valida2 = 1;
//         if (op == 4) {
//             CantCorrectas++;
//         }
//         // if (valida1 > 0 && valida2 > 0) {
//         //     var ResTot = 50 + Math.round(CantCorrectas / CantPre * 50);
//         //     $('.tres').text("Continuar");
//         //     $('#ResEvaluacion').val(ResTot);
//         //     $('#tituloAlerta').html('¡Resultado!');
//         //     $('#TextoAlerta').html('Ha respondido la evaluación y ha obtenido ' + CantCorrectas + ' respuestas correctas de ' + CantPre + '. Su puntaje para este curso es de: ' + ResTot + ' Puntos.<br><br>No olvide dar clic en CONTINUAR en la parte superior derecha para registrar el intento en la plataforma');
//         //     $('#basic-modal-content').modal();
//         // } else {
//         //     $('.tres').text("Continuar");
//         //     $('#tituloAlerta').html('¡Resultado!');
//         //     $('#TextoAlerta').html('No ha respondido todas las preguntas, por favor verifíque e intente nuevamente.');
//         //     $('#basic-modal-content').modal();
//         // }
//     }

// }

function CalcPorcentaje() {
    var vistos = 0;
    for (var i = 0; i < avance.length; i++) {
        if (avance[i] == 1) {
            vistos++;
        }
    }
    porcentaje = Math.round((vistos / cantSec) * 100);
    $('.progress-bar').text(porcentaje + ' %');
    $('.progress-bar').css('width', porcentaje + '%');
    if (porcentaje == 100) {
        $('#finalizado').val("SI");
        $('.tres').text("Continuar");
        $('#ResEvaluacion').val(100)
        completaCourse(1);

    }
}

function ValidarMenu() {
    $('#tituloAlerta').html('¡Alerta!');
    $('#TextoAlerta').html('Para navegar al siguiente modulo debe terminar el modulo donde se encuenta hubicado');
    $('#basic-modal-content').modal();
}

$('body').on('click', '.MAnimacion', function(event) {
    if ($(this).attr('id') === '6') {
        $('.caracte2').css('display', 'block')
    }
    li = parseInt($(this).attr('id')) + 1;
    $('#' + li).css('visibility', 'visible')
    if ($("li b").hasClass("numero")) {
        $('#' + $(this).attr('id') + ' .numero').css('display', 'none')
    }
    $('#' + $(this).attr('id')).removeClass('MAnimacion')
    $('#' + $(this).attr('id') + ' div').css('display', 'block')
    $('#' + $(this).attr('id') + ' p').addClass('animate__animated animate__flipInX')
    if (!$("li").hasClass("MAnimacion") && $('#finalizado').val() === 'SI') {
        setTimeout(function() {
            $('#tituloAlerta').html('¡Alerta!');
            $('#TextoAlerta').html('Finalizado, para seguir con el siguiente modulo debe navegar por el menú inicial.');
            $('#basic-modal-content').modal();
        }, 2000)

    }
});
var correcto = 0

function validar(pre, res) {
    if (pre == 1 && res == 6) { correcto++ } else if (pre == 2 && res == 9) { correcto++ } else if (pre == 3 && res == 5) { correcto++ } else if (pre == 4 && res == 8) { correcto++ } else if (pre == 5 && res == 7) { correcto++ } else if (pre == 6 && res == 2) { correcto++ } else if (pre == 7 && res == 4) { correcto++ } else if (pre == 8 && res == 11) { correcto++ } else if (pre == 9 && res == 3) { correcto++ } else if (pre == 10 && res == 12) { correcto++ } else if (pre == 11 && res == 10) { correcto++ } else if (pre == 12 && res == 1) { correcto++ } else {
        $('#tituloAlerta').html('¡Alerta!');
        $('#TextoAlerta').html('respuesta incorreccta');
        $('#basic-modal-content').modal();
    }
    if (correcto >= 12) {
        $('#taller1').removeClass('MAnimacion')
    }
}


function ValidarRespuestasEv38() {
    var cantCorrectas = 0;
    var CantPre = 6;
    if ($('#resp1').val() == 3) { cantCorrectas++; }
    if ($('#resp2').val() == 4) { cantCorrectas++; }
    if ($('#resp3').val() == 1) { cantCorrectas++; }
    if ($('#resp4').val() == 6) { cantCorrectas++; }
    if ($('#resp5').val() == 2) { cantCorrectas++; }
    if ($('#resp6').val() == 5) { cantCorrectas++; }

    var ResTot = 50 + Math.round(cantCorrectas / CantPre * 50);
    if (ResTot < 75) {
        $('.tres').text("Repetir");
        $('#ResEvaluacion').val(ResTot);
        $('#tituloAlerta').html('¡Resultado!');
        $('#TextoAlerta').html('Ha respondido la evaluación y ha obtenido ' + cantCorrectas + ' respuestas correctas de ' + CantPre + '. Su puntaje para este curso es de: ' + ResTot + ' Puntos.<br><br>No olvide dar clic en REPETIR en la parte superior derecha para registrar el intento en la plataforma');
        $('#basic-modal-content').modal();

    } else {
        $('.tres').text("Continuar");
        $('#ResEvaluacion').val(ResTot);
        $('#tituloAlerta').html('¡Resultado!');
        $('#TextoAlerta').html('Ha respondido la evaluación y ha obtenido ' + cantCorrectas + ' respuestas correctas de ' + CantPre + '. Su puntaje para este curso es de: ' + ResTot + ' Puntos.<br><br>No olvide dar clic en CONTINUAR en la parte superior derecha para registrar el intento en la plataforma');
        $('#basic-modal-content').modal();

    }

    $('#resp1').prop("disabled", true);
    $('#resp2').prop("disabled", true);
    $('#resp3').prop("disabled", true);
    $('#resp4').prop("disabled", true);
    $('#resp5').prop("disabled", true);
    $('#resp6').prop("disabled", true);
    $('#BtnValidarRespuestasEv38').prop("disabled", true);

    // $('#ResEvaluacion').val("75") // formula que calcula el porcentaje
}