// Copyright 1999-2021. Plesk International GmbH. All rights reserved.

$('body').on('click', '.entrar', function(e) {
    e.preventDefault();
    $('.piePagina').removeClass('footer')
    $('.piePagina').attr('id', 'footer')
    $('.piePagina').load('src/footerSetion.html');
    $('.content').css('border-radius', '0');
    $('.content').load('bienvenido.html', function () {
        InvocaNombreUsr();
        startIntro();
    });
    $('.pos-f-t').css('display', 'block');
});

function startIntro() {
    var intro = introJs();
    intro.setOptions({
        prevLabel: "Anterior",
        nextLabel: "Siguiente",
        skipLabel: "Saltar",
        doneLabel: "Listo",
        steps: [
            {
                element: '.dos',
                intro: "Menú contenido del modulo",
                position: 'right'
            },
            {
                element: '.tres',
                intro: "Clic para salir del curso",
                position: 'right'
            },
            {
                element: '.cuatro',
                intro: "Barra de progreso",
                position: 'right'
            },
            {
                element: '.cinco',
                intro: "Clic para avanzar en el modulo",
                position: 'right'
            },
            {
                element: '.seis',
                intro: "Menú para navegar dentro de cada modulo",
                position: 'right'
            },
            {
                element: '.siete',
                intro: "Clic para continuar",
                position: 'right'
            }
        ],
        showBullets: true,
        showProgress: true
    });

    intro.start();
    intro.oncomplete(function() {
        $('.continuar').css('display', 'block')
    });
}