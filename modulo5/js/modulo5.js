// Copyright 1999-2021. Plesk International GmbH. All rights reserved.

$(document).ready(function() {
    $('.piePagina').removeClass('footer')
    $('.piePagina').attr('id', 'footer')
    $('.piePagina').load('src/footerSetion.html', function() {
        InvocaNombreUsr();
    });
    $('.navSection').load('src/menu-contenido5.html');
    $('#precarga').show();
    jQuery(window).load(function() {
        $('#precarga').hide();
        //ya cargo la pagina 
        //Oculta o quita el gif de loading
        //Inicia los fade's
    });

});
var Secciones = ['section0.html', 'section1.html', 'section2.html', 'section3.html', 'section4.html', 'section5.html', 'section6.html', 'section7.html', 'section8.html', 'section9.html', 'section10.html', 'section11.html'],
    Avance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    CantModules = Avance.length,
    CantidadPreguntas = 2

function Ir(Modulo) {


    $('.collapse').css('visibility', 'visible')
    $('#contenido').removeClass('d-flex');
    $('#contenido').removeClass('col-12');
    $('#contenido').addClass('col-11');
    $('.navSection').css('display', 'block');
    $('.content').css('height', 'auto');
    $('.bordesolido').removeClass('btnSalir');
    $('.bordesolido').addClass('text_dorado')
    $('.tituloCabezera').css('visibility', 'visible')
    if (Modulo > 0) {
        if (Avance[Modulo - 1] == 1) {
            $('.content').css('background-color', 'white');
            $('section footer').css('background-color', 'rgb(43 46 53)');
            Avance[Modulo] = 1;
            //$('#Container').html('<div class="loading"><img src="images/loading.gif" width="110px"></div>');
            $('.conModulo').load(Secciones[Modulo]);
            $('.lista i.menuActivo').removeClass('menuActivo');
            $('#VinI' + Modulo + ' i').addClass('menuActivo');
        } else {
            $('#tituloAlerta').html('¡Alerta!');
            $('#TextoAlerta').html('Para ver esta sección debe completar la sección anterior, la navegación debe ir en forma secuencial.');
            $('#basic-modal-content').modal();
        }
    } else {
        $('.content').css('background-color', '#2b2e36');
        $('section footer').css('background-color', '#1e2126');
        Avance[Modulo] = 1;
        //$('#Container').html('<div class="loading"><img src="images/loading.gif" width="110px"></div>');
        $('.conModulo').load(Secciones[Modulo]);
        $('.lista i.menuActivo').removeClass('menuActivo');
        $('#VinI' + Modulo + ' i').addClass('menuActivo');
    }
    CalcPorcentaje();
}

function CalcPorcentaje() {
    var vistos = 0;
    for (var i = 0; i < Avance.length; i++) {
        if (Avance[i] == 1) {
            vistos++;
        }
    }
    porcentaje = Math.round((vistos / CantModules) * 100);
    $('.progress-bar').text(porcentaje + ' %');
    $('.progress-bar').css('width', porcentaje + '%');
    if (porcentaje == 100) {
        $('#finalizado').val("SI");
        $('.tres').text("Continuar");
        $('#ResEvaluacion').val(100)
        completaCourse(1);
        setTimeout(function() {
            $('#tituloAlerta').html('¡Alerta!');
            $('#TextoAlerta').html('Finalizado, para seguir con el siguiente modulo debe navegar por el menú inicial.');
            $('#basic-modal-content').modal();
        }, 2000)

    }
}

var CantCorrectas = 0;
var valida1 = 0;
var valida2 = 0;
var valida3 = 0;

function IrPre(pre, op) {
    if (pre == 1) {
        $('.conModulo').load('pregunta2.html');
        valida1 = 1;
        if (op == 3) {
            CantCorrectas++;
        }
    } else if (pre == 2) {
        $('.conModulo').load('pregunta3.html');
        valida2 = 1;
        if (op == 2) {
            CantCorrectas++;
        }
    } else if (pre == 3) {
        $('.btn_siguiente').css('display', 'block')
        valida3 = 1;
        if (op == 3) {
            CantCorrectas++;
        }
        if (valida1 > 0 && valida2 > 0 && valida3 > 0) {
            var ResTot = 50 + Math.round(CantCorrectas / CantidadPreguntas * 50);
            $('.tres').text("Continuar");
            $('#ResEvaluacion').val(ResTot);
            $('#tituloAlerta').html('¡Resultado!');
            $('#TextoAlerta').html('Ha respondido la evaluación y ha obtenido ' + CantCorrectas + ' respuestas correctas de ' + CantidadPreguntas + '. Su puntaje para este curso es de: ' + ResTot + ' Puntos.<br><br>No olvide dar clic en CONTINUAR en la parte superior derecha para registrar el intento en la plataforma');
            $('#basic-modal-content').modal();

        } else {
            $('.tres').text("Continuar");
            $('#tituloAlerta').html('¡Resultado!');
            $('#TextoAlerta').html('No ha respondido todas las preguntas, por favor verifíque e intente nuevamente.');
            $('#basic-modal-content').modal();

        }
    }
}

function ValidarMenu() {
    $('#tituloAlerta').html('¡Alerta!');
    $('#TextoAlerta').html('Para navegar al siguiente modulo debe terminar el modulo donde se encuenta hubicado');
    $('#basic-modal-content').modal();
}